package cr.ac.ucr.ecci.cql.miexamen01;

import android.provider.BaseColumns;

public class DataBaseContract {

    // Para asegurar que no se instancie la clase hacemos el constructor privado
    private DataBaseContract() {
    }

    // Definimos una clase interna que define las tablas y columnas implementa la
    // interfaz BaseColumns para heredar campos estandar del marco de Android _ID
    public static class DataBaseEntry implements BaseColumns {

        // Clase TableTop
        public static final String TABLE_NAME_TABLETOP = "TableTop";


        // private String Name;
        public static final String COLUMN_NAME_NAME = "Name";
        // private String Year;
        public static final String COLUMN_NAME_YEAR = "Year";
        // private String Publisher;
        public static final String COLUMN_NAME_PUBLISHER = "Publisher";
        // private String Country
        public static final String COLUMN_NAME_COUNTRY = "Country";
        // private String Latitude;
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        // private Date Longitude;
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        // private Date Description;
        public static final String COLUMN_NAME_DESCRIPTION = "Description";
        // private Date Number of players;
        public static final String COLUMN_NAME_NPLAYERS = "NPlayers";
        // private Date Ages;
        public static final String COLUMN_NAME_AGES = "AGES";
        // private Date PT;
        public static final String COLUMN_NAME_PT = "PT";

    }

    // Construir las tablas de la base de datos
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";

    // Creacion de tabla TableTop
    public static final String SQL_CREATE_TABLETOP =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_TABLETOP + " (" +
                    DataBaseEntry._ID + TEXT_TYPE + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_YEAR + INTEGER_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PUBLISHER + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_COUNTRY + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LATITUDE + REAL_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LONGITUDE + REAL_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_NPLAYERS + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_AGES + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PT + TEXT_TYPE + " )";

    public static final String SQL_DELETE_TABLETOP =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_TABLETOP;

}
