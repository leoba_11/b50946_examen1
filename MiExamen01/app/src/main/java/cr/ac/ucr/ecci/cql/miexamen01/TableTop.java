package cr.ac.ucr.ecci.cql.miexamen01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class TableTop implements Parcelable {

    private String identification;
    private String name;
    private int year;
    private String publisher;
    private String country;
    private Double latitude;
    private Double longitude;
    private String description;
    private String nPlayers;
    private String ages;
    private String PT;


    public TableTop() {

    }

    public TableTop(String identification, String name, int year, String publisher, String  country, Double latitude, Double longitude,
                    String description, String nPlayers, String ages, String PT) {

        this.identification = identification;
        this.name = name;
        this.year = year;
        this.publisher = publisher;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.nPlayers = nPlayers;
        this.ages = ages;
        this.PT = PT;
    }

    // Métodos de encapsulamiento de los atributos
    // Getters y Setters

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getnPlayers() {
        return nPlayers;
    }

    public void setnPlayers(String nPlayers) {
        this.nPlayers = nPlayers;
    }

    public String getAges() {
        return ages;
    }

    public void setAges(String ages) {
        this.ages = ages;
    }

    public String getPT() {
        return PT;
    }

    public void setPT(String PT) {
        this.PT = PT;
    }


    // Obtener lista de Juegos de Mesa
    public List<TableTop> getListOfTablesTop(Context context){
        List<TableTop> list = new ArrayList<>();

        // usar la clase DataBaseHelper para realizar la operacion de leer
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        // Obtiene la base de datos en modo lectura
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP + ";", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(new TableTop(cursor.getString(0), cursor.getString(1), cursor.getInt(2)
                    , cursor.getString(3), cursor.getString(4), cursor.getDouble(5)
                    , cursor.getDouble(6), cursor.getString(7), cursor.getString(8)
                    , cursor.getString(9), cursor.getString(10)
            ));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }


    public ArrayList<String> getInstanceStringArray(){
        ArrayList<String> list = new ArrayList<>();
        list.add(getIdentification());
        list.add(getName());
        list.add(Integer.toString(this.getYear()));
        list.add(getPublisher());
        list.add(getCountry());
        list.add(Double.toString(this.getLatitude()));
        list.add(Double.toString(this.getLongitude()));
        list.add(getDescription());
        list.add(getnPlayers());
        list.add(getAges());
        list.add(getPT());
        return list;
    }


    // insertar una persona en la base de datos
    public void insert(Context context) {

        // usar la clase DataBaseHelper para realizar la operacion de insertar
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);

        // Obtiene la base de datos en modo escritura
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

        // Crear un mapa de valores donde las columnas son las llaves
        ContentValues values = new ContentValues();
        values.put(DataBaseContract.DataBaseEntry._ID, getIdentification());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME, getName());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR, getYear());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER, getPublisher());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY, getCountry());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE, getLatitude());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE, getLongitude());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_DESCRIPTION, getDescription());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NPLAYERS, getnPlayers());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES, getAges());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PT, getPT());

        // Insertar la nueva fila
        db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP, null, values);
    }

    protected TableTop(Parcel in) {
        identification = in.readString();
        name = in.readString();
        year = in.readInt();
        publisher = in.readString();
        country = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        description = in.readString();
        nPlayers = in.readString();
        ages = in.readString();
        PT = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(identification);
        dest.writeString(name);
        dest.writeInt(year);
        dest.writeString(publisher);
        dest.writeString(country);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(description);
        dest.writeString(nPlayers);
        dest.writeString(ages);
        dest.writeString(PT);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TableTop> CREATOR = new Creator<TableTop>() {
        @Override
        public TableTop createFromParcel(Parcel in) {
            return new TableTop(in);
        }

        @Override
        public TableTop[] newArray(int size) {
            return new TableTop[size];
        }
    };

    @Override
    public String toString() {
        return this.name;
    }

}
