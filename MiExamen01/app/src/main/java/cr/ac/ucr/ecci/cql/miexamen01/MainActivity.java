package cr.ac.ucr.ecci.cql.miexamen01;

import androidx.appcompat.app.AppCompatActivity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import android.widget.ListView;


import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<TableTop> tableTopList;
    ListView listView;

    private List<Integer> imageID;
    private List<TableTop> itemName;
    private List<String> itemDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        DeploymentScript.RunScript(getApplicationContext());

        itemName = new ArrayList<>();

        imageID = new ArrayList<>();
            imageID.add( R.drawable.catan);
            imageID.add( R.drawable.monopoly);
            imageID.add( R.drawable.eldritch);
            imageID.add( R.drawable.mtg);
            imageID.add( R.drawable.hanabi);

        itemDescription = new ArrayList<>();

        tableTopList = new TableTop().getListOfTablesTop(getApplicationContext());
        for (TableTop table : tableTopList) {
            itemName.add(table);
            itemDescription.add(table.getDescription().substring(0, 35) + "...");
        }


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.list);
        this.setListAdapter();
    }

    public void setListAdapter() {

        CustomList adapter = new CustomList(this, itemName, imageID, itemDescription);

        // asignamos el adaptador al ListView
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TableTop item = (TableTop) listView.getItemAtPosition(position);

                Bundle bundle = new Bundle();
                DetailsFragment fragment = new DetailsFragment();
                bundle.putParcelable("tableTop", item);
                fragment.setArguments(bundle);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.tableTop_details, fragment);
                ft.commit();


            }
        });
    }
}
