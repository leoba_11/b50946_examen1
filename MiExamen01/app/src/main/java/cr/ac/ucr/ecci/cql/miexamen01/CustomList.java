package cr.ac.ucr.ecci.cql.miexamen01;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomList extends ArrayAdapter<TableTop> {

    private final Activity context;
    private final List<TableTop> itemName;
    private final List<Integer> imgId;
    private final List<String> itemDescription;

    public CustomList(Activity context, List<TableTop> itemName, List<Integer> imgid, List<String> itemDescription){
        super(context, R.layout.activity_custom_list, itemName);

        this.context = context;
        this.itemName = itemName;
        this.imgId = imgid;
        this.itemDescription = itemDescription;
    }


    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.activity_custom_list, null, true);

        ImageView imagen = rowView.findViewById(R.id.icon);
        TextView nombre = rowView.findViewById(R.id.name);
        TextView descripcion = rowView.findViewById(R.id.description);

        nombre.setText(itemName.get(position).getName());
        imagen.setImageResource(imgId.get(position));
        descripcion.setText(itemDescription.get(position));

        return rowView;
    }


}
