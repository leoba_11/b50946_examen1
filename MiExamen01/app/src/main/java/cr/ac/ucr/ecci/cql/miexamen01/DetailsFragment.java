package cr.ac.ucr.ecci.cql.miexamen01;

import android.content.Intent;
import android.os.Bundle;

//import androidx.fragment.app.Fragment;
import android.app.Fragment;



import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class DetailsFragment extends Fragment {

    private TableTop tableTop;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        // Defines the xml file for the fragment
        Bundle bundle = getArguments();
        if(bundle != null){
            tableTop = bundle.getParcelable("tableTop");
        }

        return inflater.inflate(R.layout.fragment_details, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        TextView title = view.findViewById(R.id.fragment_name);
        TextView details = view.findViewById(R.id.fragment_description);
        ImageView map = view.findViewById(R.id.mapButton);
        TextView year = view.findViewById(R.id.year);
        TextView publisher = view.findViewById(R.id.publisher);
        TextView country = view.findViewById(R.id.country);
        TextView nPlayers = view.findViewById(R.id.nPlayers);
        TextView ages = view.findViewById(R.id.ages);
        TextView pt = view.findViewById(R.id.pt);
        TextView latitude = view.findViewById(R.id.latitude);
        TextView longitude = view.findViewById(R.id.longitude);


        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MapsActivity.class);
                intent.putExtra("lat", tableTop.getLatitude());
                intent.putExtra("lon", tableTop.getLongitude());
                intent.putExtra("name", tableTop.getName());
                startActivity(intent);
            }

        });

        title.setText(tableTop.getName());
        details.setText(tableTop.getDescription());
        year.setText("YEAR: " + tableTop.getYear());
        publisher.setText("PUBLISHER: " + tableTop.getPublisher());
        country.setText("COUNTRY: " + tableTop.getCountry());
        nPlayers.setText("NUMBER OF PLAYERS: " + tableTop.getnPlayers());
        ages.setText("AGES: " + tableTop.getAges());
        pt.setText("PLAYING TIME: " + tableTop.getPT());
        latitude.setText("LATITUDE: " + tableTop.getLatitude());
        longitude.setText("LONGITUDE: " + tableTop.getLongitude());


    }
}
