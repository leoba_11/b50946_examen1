package cr.ac.ucr.ecci.cql.miexamen01;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DeploymentScript {

    public static void RunScript(Context context) {
        clearDatabase(context);
        createTableTops(context);
    }

    private static void clearDatabase(Context context) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        dataBaseHelper.onCreate(db);
    }

    private static void createTableTops(Context context) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        TableTop Catan = new TableTop("TT001", "Catan", 1995, "Kosmos", "Germany", 48.774538,
                9.188467, "Picture yourself in the era of discoveries: after a long voyage of great deprivation, your ships " +
                "have finally reached the coast of an uncharted island. Its name shall be Catan! But you are not the only discoverer. Other " +
                "fearless seafarers have also landed on the shores of Catan: the race to settle the island has begun!", "3-4", "10+", "1-2 hours");
        Catan.insert(context);

        TableTop Monopoly = new TableTop("TT002", "Monopoly", 1935, "Hasbro", "United States", 41.883736,
                -71.352259, "The thrill of bankrupting an opponent, but it pays to play nice, because fortunes could change with the " +
                "roll of the dice. Experience the ups and downs by collecting property colors sets to build houses, and maybe even upgrading to a hotel!",
                "2-8", "8+", "20-180 minutes");
        Monopoly.insert(context);

        TableTop EldritchHorror = new TableTop("TT003", "Eldritch Horror", 2013, "Fantasy Flight Games", "United States", 45.015417,
                -93.183995, "An ancient evil is stirring. You are part of a team of unlikely heroes engaged in an international struggle to stop the gathering darkness." +
                " To do so, you’ll have to defeat foul monsters, travel to Other Worlds, and solve obscure mysteries surrounding this\n" +
                "unspeakable horror.", "1-8", "14+", "2-4 hours");
        EldritchHorror.insert(context);

        TableTop Magic = new TableTop("TT004", "Magic: the Gathering", 1993, "Hasbro", "United States", 41.883736,
                -71.352259, "Magic: The Gathering is a collectible and digital collectible card game created by Richard Garfield. " +
                "Each game of Magic represents a battle between wizards known as planeswalkers who cast spells, use artifacts,\n" +
                "and summon creatures.", "2+", "13+", "Varies");
        Magic.insert(context);

        TableTop Hanabi = new TableTop("TT005", "Hanabi", 2010, "Asmodee", "France", 48.761629,
                2.065296, "Hanabi—named for the Japanese word for\n" +
                "   \"fireworks\"—is a cooperative game in which\n" +
                "  players try to create the perfect fireworks\n" +
                "show by placing the cards on the table in the\n" +
                "right order.", "2-5", "8+", "25 minutes");
        Hanabi.insert(context);

        db.close();
    }


}
